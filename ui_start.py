#!/usr/bin/python
import openni
import numpy as np
import Tkinter, ImageTk
import Image
import time
import thread
import matplotlib.pyplot as plt
import socket
import png
import os
from datetime import date

depth = openni.DepthGenerator()
rgb = openni.ImageGenerator()
ctx = openni.Context()


THRESHOLD_DEPTH = 1300
TOTAL_POTHOLES = 0
SQUARE_SIZE = 5
count = 0
prev_max_dep = 0
PIC_FLAG =0
TCP_IP = '127.0.0.1'
TCP_PORT = 50000
GPS = ""
gps_stream = 0
def updategps():
    global gps_stream,GPS
    GPS_BUF = ''
    gps_stream.connect((TCP_IP, TCP_PORT))
    while(1):
#         print(gps_stream.recv(6))
        if(gps_stream.recv(6)=='GPGGA,'):
            print("ENTERED")
            while(gps_stream.recv(1)!=','):
                pass
            temp = gps_stream.recv(1)
            while(temp!=','):
                GPS_BUF = ''
                GPS_BUF = GPS_BUF + temp
                temp = gps_stream.recv(1)
            GPS_BUF = GPS_BUF + " "
            temp = gps_stream.recv(1)
            if(temp != ','):
                GPS_BUF = GPS_BUF + temp
            GPS_BUF = GPS_BUF + " "
            while(temp!=','):
                GPS_BUF = GPS_BUF + temp
                temp = gps_stream.recv(1)
            GPS_BUF = GPS_BUF + " "
            if(temp != ','):
                GPS_BUF = GPS_BUF + temp
            if(GPS_BUF != ''): GPS = GPS_BUF
            print("GPS DATA WRITTEN" + GPS)
#             gps_stream.close()
def initiate():
    global rgb,depth,ctx,gps_stream
    gps_stream = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ctx.init() 
    # Create a depth generator
    depth = openni.DepthGenerator()
    depth.create(ctx)
    
    rgb = openni.ImageGenerator()
    rgb.create(ctx)
    # Set it to VGA maps at 30 FPS
    depth.set_resolution_preset(openni.RES_VGA)
    rgb.set_resolution_preset(openni.RES_VGA)
    depth.fps = 30
    rgb.fps = 30
    ctx.start_generating_all()
    thread.start_new_thread( updategps, () )

def takepic(img,depth_data):
    global TOTAL_POTHOLES,count,GPS
    name = "Pothole#"+str(date.today().year)+str(date.today().month)+str(date.today().day)+str(time.localtime().tm_hour)+str(time.localtime().tm_min)+str(time.localtime().tm_sec)
    if(count == 0):
        try:
            os.system("mkdir Pictures_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)))
        except:
            pass
        count = 1
    os.system("cd Pictures_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)) + "&& mkdir %s"%name)
    img.save("Pictures_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)) + "/%s/Pothole.jpg"%name)
    pic = open("Pictures_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)) + "/%s/depth.png"%name,'w')
    gps_data = open("Pictures_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)) + "/%s/gps.dat"%name,'w')
    png.from_array(depth_data.astype(np.int16),'L').save(pic)
    gps_data.write(GPS)
    gps_data.close()
    pic.close()


    
    #Also arrange to save gps co-ordinates here
    
def is_square_down(data,index,pixel_data):

    global SQUARE_SIZE
    # print data.shape
    y = index / 640
    x = index - (640*y)
#     print(y,x)
    if(x-SQUARE_SIZE < 0):
        left = 0
    else: 
        left = x-SQUARE_SIZE
    if(x+SQUARE_SIZE > 640):
        right = 640
    else: 
        right = x+SQUARE_SIZE
    if(y-SQUARE_SIZE < 0):
        top = 0
    else: 
        top = y-SQUARE_SIZE
    if(y+SQUARE_SIZE > 480):
        bottom = 480
    else: 
        bottom = y+SQUARE_SIZE
    for j in xrange(left,right):
        for i in xrange(top,bottom):
#             print "data[" + str(j)+"]["+str(i)+"]"
            pixel_data[j,i] = (38,176,222);
            if (data[i][j]<THRESHOLD_DEPTH):
                return False,pixel_data
    return True,pixel_data

def is_square_up(data,index,pixel_data):

    global SQUARE_SIZE
    # print data.shape
    y = index / 640
    x = index - (640*y)
#     print(y,x)
    if(x-SQUARE_SIZE < 0):
        left = 0
    else: 
        left = x-SQUARE_SIZE
    if(x+SQUARE_SIZE > 640):
        right = 640
    else: 
        right = x+SQUARE_SIZE
    if(y-SQUARE_SIZE < 0):
        top = 0
    else: 
        top = y-SQUARE_SIZE
    if(y+SQUARE_SIZE > 480):
        bottom = 480
    else: 
        bottom = y+SQUARE_SIZE
    for j in xrange(left,right):
        for i in xrange(top,bottom):
#             print "data[" + str(j)+"]["+str(i)+"]"
            pixel_data[j,i] = (255,0,0);
            if (data[i][j]>THRESHOLD_DEPTH):
                return False,pixel_data
    return True,pixel_data




class App(Tkinter.Tk):
    def __init__(self):
        Tkinter.Tk.__init__(self)
        global TOTAL_POTHOLES
        self.we_in_pothole = False
        
        self.label1 = Tkinter.Label(text="your image here", compound="top")
        #self.label1.pack(side=Tkinter.LEFT, padx=8, pady=8)
        self.label1.grid(row=0,column=1)
        self.label2 = Tkinter.Label(text="secondImage", compound="top")
        #self.label2.pack(side=Tkinter.LEFT, padx=8, pady=8)
        self.label2.grid(row=0,column=2)
        self.label3 = Tkinter.Label(text="GPS_data", compound="top")
        self.label3.grid(row=1,column=1)
        self.button = Tkinter.Button(text="Get Depth", command=self.save_data)
        self.button.grid(row=2,column=2)
        #self.label3.pack(side=Tkinter.BOTTOM, padx=8, pady=8)
        self.label4 = Tkinter.Label(text="Max_depth", compound="top")
        self.label4.grid(row=1,column=2)
        self.iteration=0
        self.UpdateUi(0)
    
    def __exit__(self):
        global gps_stream
        gps_stream.close()
    
    def save_data(self):
        global GPS
        name = "Clikpik#"+str(date.today().year)+str(date.today().month)+str(date.today().day)+str(time.localtime().tm_hour)+str(time.localtime().tm_min)+str(time.localtime().tm_sec)
        try:
            os.system("mkdir ClikPiks_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)))
        except:
            pass
        os.system("cd ClikPiks_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)) + "&& mkdir %s"%name)
        self.rgb_img.save("ClikPiks_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)) + "/%s/Pic.jpg"%name)
        pic = open("ClikPiks_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)) + "/%s/depth.png"%name,'w')
        png.from_array(self.depth_data.astype(np.int16),'L').save(pic)
        pic.close()
        gps_data = open("ClikPiks_%s"%(str(date.today().day)+str(date.today().month)+str(date.today().year)) + "/%s/gps.dat"%name,'w')
        gps_data.write(GPS)
        gps_data.close()

    def UpdateUi(self, delay, event=None):
        # this is merely so the display changes even though the image doesn't
        self.iteration += 1

        self.image1,self.image2 = self.get_data()
        self.label1.configure(image=self.image1, text="Iteration %s" % self.iteration)
        self.label2.configure(image=self.image2, text="Registered PHs %s" % TOTAL_POTHOLES)
        self.label3.configure(text="GPS_data\n %s" % "waiting")
        self.label4.configure(text="Max_depth\n %s mm" % str(self.max_dep))
        # reschedule to run again in 1 second
        self.after(delay, self.UpdateUi,1)

    def get_data(self):
        global rgb,depth,THRESHOLD_DEPTH,TOTAL_POTHOLES,prev_max_dep,PIC_FLAG
        ctx.wait_one_update_all(depth)
        rgb_img_data = rgb.get_synced_image_map()
        depth_img_data = depth.get_raw_depth_map_8()
        #self.depth_data = depth.get_tuple_depth_map()
        self.rgb_img = Image.fromstring('RGB', (640,480), rgb_img_data)
        depth_img = Image.fromstring('L', (640,480), depth_img_data)
        pixel_data = self.rgb_img.load()
        
        self.depth_data = np.asanyarray(a = depth.get_tuple_depth_map()).astype(np.int16)
#         self.max_index = np.argmax(self.depth_data)
        self.depth_data = self.depth_data.reshape(480,640)
        maxdep = 0
        for i in range(48,432):
            for j in range(64,576):
                if(maxdep<self.depth_data[i][j]):
                    maxdep = self.depth_data[i][j]
                    self.max_index = ((i)*640) + j
        self.max_dep = maxdep

        
        
        is_square_down_now,pixel_data = is_square_down(self.depth_data,self.max_index,pixel_data)
        is_square_up_now,pixel_data = is_square_up(self.depth_data,self.max_index,pixel_data)
#         if(is_square_down_now): print"It did return true"
        
        if(self.we_in_pothole and PIC_FLAG==0):
            PIC_FLAG = 1
            print("Taking a Pic")
            takepic(self.rgb_img,self.depth_data)
        
        if(self.we_in_pothole and is_square_up_now == True): # We are'nt in the pothole anymore !!
            print("We outrtta pothole!!!!")
            PIC_FLAG=0
            self.we_in_pothole = False
        
        if(is_square_down_now and self.we_in_pothole == False): # We've just entered the pothole
            print("Jus enetered pothole")
            self.we_in_pothole = True
            TOTAL_POTHOLES += 1
            prev_max_dep = self.max_dep
        
        
        #self.max_index, self.max_dep = max(enumerate(self.depth_data), key=operator.itemgetter(1))
        
        image1 = ImageTk.PhotoImage(self.rgb_img)
        image2 = ImageTk.PhotoImage(depth_img)
        return image1,image2

if __name__ == "__main__":
    
    initiate()
    app=App()
    app.mainloop()


